# server to perform backup and restore of a path or a docker volume container
#FROM phusion/baseimage
FROM alpine
MAINTAINER j842
#RUN apt-get update ; apt-get install -y wget ; rm -rf /var/lib/apt/lists/*

RUN apk add --update wget ca-certificates

ADD ["./assets","/"]
CMD ["/bin/update.sh"]
